import { LitElement, html, css } from 'lit-element';
import '@material/mwc-textfield';
import '@material/mwc-button';
import '@material/mwc-icon-button';
import '@material/mwc-list';
import '@material/mwc-list/mwc-check-list-item';
import '@material/mwc-checkbox';
import '@material/mwc-top-app-bar'

class SecretSant extends LitElement {
  static get styles() {
    return css`
        
      .container {
        display: flex;
        flex-direction: column;
        margin: 0 auto;
        width: 50vw;
        max-width: 70vw;
      }
      mwc-top-app-bar {
        --mdc-theme-primary: orange;
        --mdc-theme-on-primary: black;
      }
      mwc-textfield {
        --mdc-theme-primary: orange;
      }
      mwc-button {
        --mdc-theme-primary: #ffa500;
        --mdc-theme-on-primary: back;
      }
      .inverted {
        background-color: gray;
        color: white;
      }
    `;
  }

  static get properties() {
    return {
      noUserData: {
        type: Boolean,
      },
      user: {
        type: String,
      },
      users: {
        type: Array,
      },
    };
  }

  constructor() {
    super();
    this.user = '';
    this.users = [];
    this.noTodoData = true;
  }

  addUser() {
    const user = this.shadowRoot.querySelector('#current-user');
    this.users = [...this.users, user.value];
    // Esta función se ejecuta si el spread opereator no es suficiente
    // como para que el cambio se refleje de forma atomática
    // this.requestUpdate()
    user.value = '';
  }



  render() {
    return html`
       
            <mwc-top-app-bar centerTitle>
                <mwc-icon-button icon="menu" slot="navigationIcon"></mwc-icon-button>
                <div slot="title">Secret Santa</div>
                <mwc-icon-button icon="favorite" slot="actionItems"></mwc-icon-button>
            </mwc-top-app-bar>
  

      <div class="container">
        <mwc-textfield
          label="Participante"
          helper="Escribe aquí al Participante"
          @change="${(e) => (this.user = e.target.value)}"
          .value="${this.user}"
          id="current-user"
        ></mwc-textfield>
        <mwc-button
          raised
          @click="${this.addUser}"
          label="Agregar"
          ?disabled="${this.user === ''}"
        ></mwc-button>
    </div>

    <div class="container">
        ${this.users.length === 0
          ? html` <p>Participantes</p> `
          : html`
          <mwc-list>
            <mwc-list-item twoline graphic="avatar" noninteractive>
            <span>User Name</span>
            <mwc-icon slot="graphic" class="inverted">tag_faces</mwc-icon>
          </mwc-list-item>
             
          <mwc-textfield
          label="Regalo"
          helper="Escribe aquí tu Regalo"
          @change="${(e) => (this.user = e.target.value)}"
          .value="${this.user}"
          id=""
        ></mwc-textfield>
        <mwc-button
          raised
          @click="${this.addUser}"
          label="Agregar"
          ?disabled="${this.user === ''}"
        ></mwc-button>
            `}
      </div>
    `;
  }
}

customElements.define('secret-santa-element', SecretSant);